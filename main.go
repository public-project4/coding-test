package main

import (
	"fmt"
	service "quitz/service"
)

func main() {
	roman := "III"
	output, explanation, err := service.ReadRoman("III")

	if err != nil {
		fmt.Println("ERROR")
	} else {
		fmt.Printf("Input: s = \"%s\" \n", roman)
		fmt.Printf("Output: %d \n", output)
		fmt.Printf("Explanation: %s \n", explanation)
		fmt.Println("-------------")
	}

}
