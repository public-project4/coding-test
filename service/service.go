package service

import (
	"fmt"
)

type explanModel struct {
	key       string
	value     int
	joinRoman string
}

var romanCharMap = map[byte]int{
	'I': 1,
	'V': 5,
	'X': 10,
	'L': 50,
	'C': 100,
	'D': 500,
	'M': 1000,
}

func ReadRoman(roman string) (int, string, error) {

	err := validate(roman)
	if err != nil {
		return 0, "", err
	}

	resultValue, prevValue, j, keyValue := 0, 0, 0, 0
	var prevChar, explaination, joinRoman, key string
	explainMap := map[int]explanModel{}

	for i := len(roman) - 1; i >= 0; i-- {
		romanChar := roman[i]
		value, valid := romanCharMap[romanChar]
		if !valid {
			return 0, "", fmt.Errorf(ErrorMsg_Invalid, roman)
		}
		if value < prevValue {
			resultValue -= value
			if len(explainMap) > 0 {
				j--
				key = fmt.Sprintf("%s%s", string(romanChar), prevChar)
				resultValue2 := resultValue
				for i := 0; i <= len(explainMap)-2; i++ {
					prevMap := explainMap[i]
					resultValue2 -= prevMap.value
				}
				keyValue = resultValue2

			}
		} else {
			resultValue += value
			if value == prevValue {
				j--
				prevMap := explainMap[j]
				joinRoman = string(romanChar) + prevChar
				key = fmt.Sprintf("%s%s", string(romanChar), prevMap.joinRoman)
				keyValue = value + prevMap.value
			} else {
				joinRoman = string(romanChar)
				key = string(romanChar)
				keyValue = value
			}

		}
		prevValue = value
		explainMap[j] = explanModel{key, keyValue, joinRoman}
		j++

		prevChar = string(romanChar)
	}
	for i := len(explainMap) - 1; i >= 0; i-- {
		eachExplain := explainMap[i]
		if len(explainMap) > 1 && i < len(explainMap)-1 {
			current := explainMap[i]
			prev := explainMap[i+1]
			if current.value > prev.value {
				return 0, "", fmt.Errorf(ErrorMsg_Invalid, roman)
			}
		}
		explaination += fmt.Sprintf("%s = %d", eachExplain.key, eachExplain.value) + " "
	}

	if resultValue > 3999 {
		return 0, "", fmt.Errorf(ErrorMsg_OutOfRange, roman)
	}
	return resultValue, explaination, nil
}

func validate(input string) error {

	inputLength := len(input)
	if inputLength < 1 {
		return fmt.Errorf(ErrorMsg_EmptyInput)
	}
	if inputLength > 15 {
		return fmt.Errorf(ErrorMsg_OutOfRange, input)
	}

	return nil
}
