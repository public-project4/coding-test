package service

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ReadRomanEx1(t *testing.T) {
	roman := "III"
	output, explanation, err := ReadRoman(roman)

	if err != nil {
		t.Fatalf("error msg: %s", err)
	}

	printResult(roman, explanation, output)
}

func Test_ReadRomanEx2(t *testing.T) {
	roman := "LVIII"
	output, explanation, err := ReadRoman(roman)

	if err != nil {
		t.Fatalf("error msg: %s", err)
	}

	printResult(roman, explanation, output)
}

func Test_ReadRomanEx3(t *testing.T) {
	roman := "MCMXCIV"
	output, explanation, err := ReadRoman(roman)

	if err != nil {
		t.Fatalf("error msg: %s", err)
	}

	printResult(roman, explanation, output)
}

func Test_ReadRoman_InvalidCharactor(t *testing.T) {
	input := "XIF"
	_, _, err := ReadRoman(input)

	assert.Equal(t, err.Error(), ToErrMsg(ErrorMsg_Invalid, input))
}

func Test_ReadRoman_OutOfRange(t *testing.T) {
	input := "XXXXXXXXXXXXXXXX"
	_, _, err := ReadRoman(input)

	assert.Equal(t, err.Error(), ToErrMsg(ErrorMsg_OutOfRange, input))
}

func Test_ReadRoman_EmptyInput(t *testing.T) {
	input := ""
	_, _, err := ReadRoman(input)

	assert.Equal(t, err.Error(), ToErrMsg(ErrorMsg_EmptyInput, input))
}

func Test_ReadRoman_MoreThan3999(t *testing.T) {
	input := "MMMMM"
	_, _, err := ReadRoman(input)

	assert.Equal(t, err.Error(), ToErrMsg(ErrorMsg_OutOfRange, input))
}

func ToErrMsg(format, input string) string {
	if strings.Count(format, "%s") == 1 {
		return fmt.Sprintf(format, input)
	} else {
		return format
	}

}

func printResult(roman, explanation string, output int) {
	fmt.Printf("Input: s = \"%s\" \n", roman)
	fmt.Printf("Output: %d \n", output)
	fmt.Printf("Explanation: %s \n", explanation)
	fmt.Println("-------------")
}
